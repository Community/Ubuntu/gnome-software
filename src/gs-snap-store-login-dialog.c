/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Copyright (C) 2020 Canonical Ltd
 *
 * SPDX-License-Identifier: GPL-2.0+
 */

#include "config.h"

#include "gs-snap-store-login-dialog.h"

#include "gnome-software-private.h"
#include "gs-common.h"
#include "gs-snap-auth.h"
#include <glib/gi18n.h>
#include <snapd-glib/snapd-glib.h>

struct _GsSnapStoreLoginDialog
{
	GtkDialog parent_instance;

	GtkLabel       *auth_error_label;
	GtkButton      *continue_button;
	GtkEntry       *email_entry;
	GtkRadioButton *existing_account_radio;
	GtkLabel       *forgot_password_label;
	GtkLabel       *legal_label;
	GtkEntry       *password_entry;
	GtkRadioButton *register_account_radio;
	GtkStack       *stack;
	GtkEntry       *two_factor_entry;
	GtkLabel       *two_factor_error_label;
	GtkEntry       *two_factor_grid;

	GCancellable   *cancellable;
};

G_DEFINE_TYPE (GsSnapStoreLoginDialog, gs_snap_store_login_dialog, GTK_TYPE_DIALOG)

static void
login_cb (GObject *object, GAsyncResult *result, gpointer user_data)
{
	GsSnapStoreLoginDialog *self = user_data;
	SnapdUserInformation *user_info;
	SnapdAuthData *auth_data;
	g_autoptr(GError) error = NULL;

	user_info = snapd_client_login2_finish (SNAPD_CLIENT (object), result, &error);
	if (user_info == NULL) {
		const gchar *error_text;

		if (g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
			return;

		gtk_widget_set_sensitive (GTK_WIDGET (self->stack), TRUE);

		if (g_error_matches (error, SNAPD_ERROR, SNAPD_ERROR_TWO_FACTOR_REQUIRED)) {
			gtk_stack_set_visible_child (self->stack, GTK_WIDGET (self->two_factor_grid));
			gtk_widget_grab_focus (GTK_WIDGET (self->two_factor_entry));
			return;
		}

		if (g_error_matches (error, SNAPD_ERROR, SNAPD_ERROR_TWO_FACTOR_INVALID)) {
			gtk_label_set_label (self->two_factor_error_label, _("Invalid code"));
			gtk_widget_show (GTK_WIDGET (self->two_factor_error_label));
			gtk_widget_grab_focus (GTK_WIDGET (self->two_factor_entry));
			return;
		}

		// FIXME: Handle polkit cancelled
		if (g_error_matches (error, SNAPD_ERROR, SNAPD_ERROR_AUTH_DATA_INVALID) ||
		    g_error_matches (error, SNAPD_ERROR, SNAPD_ERROR_AUTH_DATA_REQUIRED))
			error_text = _("Incorrect email/password combination");
		else
			error_text = error->message;

		gtk_label_set_label (self->auth_error_label, error_text);
		gtk_widget_show (GTK_WIDGET (self->auth_error_label));
		gtk_widget_grab_focus (GTK_WIDGET (self->email_entry));
		return;
	}

	auth_data = snapd_user_information_get_auth_data (user_info);
	gs_snap_auth_save (snapd_user_information_get_id (user_info),
	                   snapd_user_information_get_username (user_info),
	                   snapd_user_information_get_email (user_info),
	                   auth_data != NULL ? snapd_auth_data_get_macaroon (auth_data) : NULL,
	                   auth_data != NULL ? snapd_auth_data_get_discharges (auth_data) : NULL,
	                   self->cancellable);

	gtk_dialog_response (GTK_DIALOG (self), GTK_RESPONSE_OK);
}

static void
login (GsSnapStoreLoginDialog *self)
{
	const gchar *email, *password, *second_factor;
	g_autoptr(SnapdClient) snapd_client = NULL;

	email = gtk_entry_get_text (self->email_entry);
	password = gtk_entry_get_text (self->password_entry);
	second_factor = gtk_entry_get_text (self->two_factor_entry);
	if (g_str_equal (second_factor, ""))
		second_factor = NULL;

	snapd_client = snapd_client_new ();
	snapd_client_login2_async (snapd_client, email, password, second_factor, self->cancellable, login_cb, self);

	gtk_widget_set_sensitive (GTK_WIDGET (self->stack), FALSE);
	gtk_widget_set_sensitive (GTK_WIDGET (self->continue_button), FALSE);
}

static void
show_uri (GsSnapStoreLoginDialog *self, const gchar *uri)
{
	g_autoptr(GError) error = NULL;

	if (!gtk_show_uri_on_window (GTK_WINDOW (self),
	                             uri,
	                             GDK_CURRENT_TIME,
	                             &error)) {
		g_warning ("failed to show URI %s: %s", uri, error->message);
	}
}

static void
cancel_cb (GsSnapStoreLoginDialog *self)
{
	gtk_dialog_response (GTK_DIALOG (self), GTK_RESPONSE_CANCEL);
}

static void
continue_cb (GsSnapStoreLoginDialog *self)
{
	if (gtk_stack_get_visible_child (self->stack) == GTK_WIDGET (self->two_factor_grid) ||
	    gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (self->existing_account_radio))) {
		login (self);
	} else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (self->register_account_radio))) {
		show_uri (self, "https://login.ubuntu.com/+login");
	}
}

static void
email_entry_activate_cb (GsSnapStoreLoginDialog *self)
{
	if (strlen (gtk_entry_get_text (self->email_entry)) > 0)
		gtk_widget_grab_focus (GTK_WIDGET (self->password_entry));
}

static void
values_changed_cb (GsSnapStoreLoginDialog *self)
{
	gboolean can_continue = TRUE;

	if (gtk_stack_get_visible_child (self->stack) == GTK_WIDGET (self->two_factor_grid)) {
		const gchar *second_factor;

		second_factor = gtk_entry_get_text (self->two_factor_entry);
		can_continue = strlen (second_factor) > 0;
	} if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (self->existing_account_radio))) {
		const gchar *email, *password;

		email = gtk_entry_get_text (self->email_entry);
		password = gtk_entry_get_text (self->password_entry);
		can_continue = strlen (email) > 0 && strlen (password) > 0;
	}

	gtk_widget_set_sensitive (GTK_WIDGET (self->continue_button), can_continue);
}

static void
gs_snap_store_login_dialog_dispose (GObject *object)
{
	GsSnapStoreLoginDialog *self = GS_SNAP_STORE_LOGIN_DIALOG (object);

	g_cancellable_cancel (self->cancellable);
	g_clear_object (&self->cancellable);

	G_OBJECT_CLASS (gs_snap_store_login_dialog_parent_class)->dispose (object);
}

static void
gs_snap_store_login_dialog_init (GsSnapStoreLoginDialog *self)
{
	g_autofree gchar *forgot_password_link = NULL;
	g_autofree gchar *legal_link = NULL;
	g_autofree gchar *email = NULL;

	self->cancellable = g_cancellable_new ();

	gtk_widget_init_template (GTK_WIDGET (self));

	forgot_password_link = g_strdup_printf ("<a href=\"https://login.ubuntu.com/+forgot_password\">%s</a>", _("I have forgotten my password"));
	gtk_label_set_markup (self->forgot_password_label, forgot_password_link);
	legal_link = g_strdup_printf ("<a href=\"https://www.ubuntu.com/legal/dataprivacy/snap-store\">%s</a>", _("About Snap Store privacy"));
	gtk_label_set_markup (self->legal_label, legal_link);

	if (gs_snap_auth_load (NULL, NULL, &email, NULL, NULL)) {
		if (email != NULL)
			gtk_entry_set_text (self->email_entry, email);
	}

	values_changed_cb (self);

	gtk_widget_grab_focus (GTK_WIDGET (self->email_entry));
}

static void
gs_snap_store_login_dialog_class_init (GsSnapStoreLoginDialogClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

	object_class->dispose = gs_snap_store_login_dialog_dispose;

	gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Software/gs-snap-store-login-dialog.ui");

	gtk_widget_class_bind_template_child (widget_class, GsSnapStoreLoginDialog, auth_error_label);
	gtk_widget_class_bind_template_child (widget_class, GsSnapStoreLoginDialog, continue_button);
	gtk_widget_class_bind_template_child (widget_class, GsSnapStoreLoginDialog, email_entry);
	gtk_widget_class_bind_template_child (widget_class, GsSnapStoreLoginDialog, existing_account_radio);
	gtk_widget_class_bind_template_child (widget_class, GsSnapStoreLoginDialog, forgot_password_label);
	gtk_widget_class_bind_template_child (widget_class, GsSnapStoreLoginDialog, legal_label);
	gtk_widget_class_bind_template_child (widget_class, GsSnapStoreLoginDialog, password_entry);
	gtk_widget_class_bind_template_child (widget_class, GsSnapStoreLoginDialog, register_account_radio);
	gtk_widget_class_bind_template_child (widget_class, GsSnapStoreLoginDialog, stack);
	gtk_widget_class_bind_template_child (widget_class, GsSnapStoreLoginDialog, two_factor_entry);
	gtk_widget_class_bind_template_child (widget_class, GsSnapStoreLoginDialog, two_factor_error_label);
	gtk_widget_class_bind_template_child (widget_class, GsSnapStoreLoginDialog, two_factor_grid);

	gtk_widget_class_bind_template_callback (widget_class, cancel_cb);
	gtk_widget_class_bind_template_callback (widget_class, continue_cb);
	gtk_widget_class_bind_template_callback (widget_class, email_entry_activate_cb);
	gtk_widget_class_bind_template_callback (widget_class, values_changed_cb);
}

GsSnapStoreLoginDialog *
gs_snap_store_login_dialog_new (GtkWindow *parent)
{
	return g_object_new (GS_TYPE_SNAP_STORE_LOGIN_DIALOG,
			     "use-header-bar", TRUE,
			     "transient-for", parent,
			     "modal", TRUE,
			     NULL);
}

void
gs_snap_store_login_dialog_set_email (GsSnapStoreLoginDialog *self, const gchar *email)
{
	g_return_if_fail (GS_IS_SNAP_STORE_LOGIN_DIALOG (self));
	gtk_entry_set_text (self->email_entry, email);
}

const gchar *
gs_snap_store_login_dialog_get_email (GsSnapStoreLoginDialog *self)
{
	g_return_val_if_fail (GS_IS_SNAP_STORE_LOGIN_DIALOG (self), NULL);
	return gtk_entry_get_text (self->email_entry);
}

const gchar *
gs_snap_store_login_dialog_get_macaroon (GsSnapStoreLoginDialog *self)
{
	g_return_val_if_fail (GS_IS_SNAP_STORE_LOGIN_DIALOG (self), NULL);
	return NULL;
}

GStrv
gs_snap_store_login_dialog_get_discharges (GsSnapStoreLoginDialog *self)
{
	g_return_val_if_fail (GS_IS_SNAP_STORE_LOGIN_DIALOG (self), NULL);
	return NULL;
}
