/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Copyright (C) 2020 Canonical Ltd
 *
 * SPDX-License-Identifier: GPL-2.0+
 */

#include "config.h"

#include "gs-snap-store-logout-dialog.h"

#include "gnome-software-private.h"
#include "gs-common.h"
#include "gs-snap-auth.h"
#include <glib/gi18n.h>
#include <snapd-glib/snapd-glib.h>

struct _GsSnapStoreLogoutDialog
{
	GtkDialog parent_instance;

	GtkLabel       *account_label;
	GtkLabel       *confirmation_label;
	GtkButton      *logout_button;
	GtkStack       *stack;

	GCancellable   *cancellable;
};

G_DEFINE_TYPE (GsSnapStoreLogoutDialog, gs_snap_store_logout_dialog, GTK_TYPE_DIALOG)

static void
logout_finished_cb (GObject *object, GAsyncResult *result, gpointer user_data)
{
	GsSnapStoreLogoutDialog *self;
	g_autoptr(GError) error = NULL;

	if (!snapd_client_logout_finish (SNAPD_CLIENT (object), result, &error)) {
		if (g_error_matches (error, G_IO_ERROR, G_IO_ERROR_CANCELLED))
			return;
		g_warning ("Failed to logout: %s", error->message);
	}

	self = GS_SNAP_STORE_LOGOUT_DIALOG (user_data);

	gs_snap_auth_remove ();

	gtk_dialog_response (GTK_DIALOG (self), GTK_RESPONSE_OK);
}

static void
logout (GsSnapStoreLogoutDialog *self)
{
	gint64 id;
	g_autofree gchar *macaroon = NULL;
	g_auto(GStrv) discharges = NULL;
	g_autoptr(SnapdClient) snapd_client = NULL;
	g_autoptr(SnapdAuthData) auth_data = NULL;

	if (!gs_snap_auth_load (&id, NULL, NULL, &macaroon, &discharges))
		return;

	snapd_client = snapd_client_new ();
	auth_data = snapd_auth_data_new (macaroon, discharges);
	snapd_client_set_auth_data (snapd_client, auth_data);
	snapd_client_logout_async (snapd_client, id, self->cancellable, logout_finished_cb, self);
}

static void
cancel_cb (GsSnapStoreLogoutDialog *self)
{
	gtk_dialog_response (GTK_DIALOG (self), GTK_RESPONSE_CANCEL);
}

static void
logout_cb (GsSnapStoreLogoutDialog *self)
{
	logout (self);
}

static void
gs_snap_store_logout_dialog_dispose (GObject *object)
{
	GsSnapStoreLogoutDialog *self = GS_SNAP_STORE_LOGOUT_DIALOG (object);

	g_cancellable_cancel (self->cancellable);
	g_clear_object (&self->cancellable);

	G_OBJECT_CLASS (gs_snap_store_logout_dialog_parent_class)->dispose (object);
}

static void
gs_snap_store_logout_dialog_init (GsSnapStoreLogoutDialog *self)
{
	g_autofree gchar *email = NULL;

	self->cancellable = g_cancellable_new ();

	gtk_widget_init_template (GTK_WIDGET (self));

	if (gs_snap_auth_load (NULL, NULL, &email, NULL, NULL)) {
		g_autofree gchar *text = NULL;

		if (email != NULL) {
			text = g_strdup_printf (_("You are signed in as %s."), email);
		} else {
			text = g_strdup (_("You are not currently signed in."));
		}

		gtk_label_set_text (self->account_label, text);
	}
}

static void
gs_snap_store_logout_dialog_class_init (GsSnapStoreLogoutDialogClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

	object_class->dispose = gs_snap_store_logout_dialog_dispose;

	gtk_widget_class_set_template_from_resource (widget_class, "/org/gnome/Software/gs-snap-store-logout-dialog.ui");

	gtk_widget_class_bind_template_child (widget_class, GsSnapStoreLogoutDialog, account_label);
	gtk_widget_class_bind_template_child (widget_class, GsSnapStoreLogoutDialog, confirmation_label);
	gtk_widget_class_bind_template_child (widget_class, GsSnapStoreLogoutDialog, logout_button);
	gtk_widget_class_bind_template_child (widget_class, GsSnapStoreLogoutDialog, stack);

	gtk_widget_class_bind_template_callback (widget_class, cancel_cb);
	gtk_widget_class_bind_template_callback (widget_class, logout_cb);
}

GsSnapStoreLogoutDialog *
gs_snap_store_logout_dialog_new (GtkWindow *parent)
{
	return g_object_new (GS_TYPE_SNAP_STORE_LOGOUT_DIALOG,
			     "use-header-bar", TRUE,
			     "transient-for", parent,
			     "modal", TRUE,
			     NULL);
}
