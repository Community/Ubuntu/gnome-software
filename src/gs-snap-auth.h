/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Copyright (C) 2020 Canonical Ltd.
 *
 * SPDX-License-Identifier: GPL-2.0+
 */

#pragma once

#include <glib.h>

G_BEGIN_DECLS

gboolean      gs_snap_auth_load    (gint64 *id, gchar **username, gchar **email, gchar **macaroon, GStrv *discharges);

void          gs_snap_auth_save    (gint64 id, const gchar *username, const gchar *email, const gchar *macaroon, GStrv discharges, GCancellable *cancellable);

void          gs_snap_auth_remove  (void);

GFileMonitor *gs_snap_auth_monitor (GCancellable *cancellable, GError **error);

G_END_DECLS
