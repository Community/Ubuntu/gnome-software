/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Copyright (C) 2020 Canonical Ltd.
 *
 * SPDX-License-Identifier: GPL-2.0+
 */

#pragma once

#include <gtk/gtk.h>

#include "gnome-software-private.h"

G_BEGIN_DECLS

#define GS_TYPE_SNAP_STORE_LOGIN_DIALOG (gs_snap_store_login_dialog_get_type ())

G_DECLARE_FINAL_TYPE (GsSnapStoreLoginDialog, gs_snap_store_login_dialog, GS, SNAP_STORE_LOGIN_DIALOG, GtkDialog)

GsSnapStoreLoginDialog	*gs_snap_store_login_dialog_new			(GtkWindow *parent);

void			 gs_snap_store_login_dialog_set_email		(GsSnapStoreLoginDialog *dialog, const gchar *email);

const gchar		*gs_snap_store_login_dialog_get_email		(GsSnapStoreLoginDialog *dialog);

const gchar		*gs_snap_store_login_dialog_get_macaroon	(GsSnapStoreLoginDialog *dialog);

GStrv			 gs_snap_store_login_dialog_get_discharges	(GsSnapStoreLoginDialog *dialog);

G_END_DECLS
