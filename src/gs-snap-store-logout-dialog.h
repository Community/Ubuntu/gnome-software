/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Copyright (C) 2020 Canonical Ltd.
 *
 * SPDX-License-Identifier: GPL-2.0+
 */

#pragma once

#include <gtk/gtk.h>

#include "gnome-software-private.h"

G_BEGIN_DECLS

#define GS_TYPE_SNAP_STORE_LOGOUT_DIALOG (gs_snap_store_logout_dialog_get_type ())

G_DECLARE_FINAL_TYPE (GsSnapStoreLogoutDialog, gs_snap_store_logout_dialog, GS, SNAP_STORE_LOGOUT_DIALOG, GtkDialog)

GsSnapStoreLogoutDialog	*gs_snap_store_logout_dialog_new		(GtkWindow *parent);

G_END_DECLS
