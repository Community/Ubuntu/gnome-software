/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Copyright (C) 2020 Canonical Ltd
 *
 * SPDX-License-Identifier: GPL-2.0+
 */

#include <glib/gstdio.h>
#include <json-glib/json-glib.h>

#include "gs-snap-auth.h"

static gchar *
get_auth_filename (void)
{
	return g_build_filename (g_get_home_dir (), ".snap", "auth.json", NULL);
}

gboolean
gs_snap_auth_load (gint64 *id, gchar **username, gchar **email, gchar **macaroon, GStrv *discharges)
{
	g_autofree gchar *filename = get_auth_filename ();
	g_autoptr(JsonParser) parser = NULL;
	JsonObject *object;
	gint64 id_ = 0;
	const gchar *username_ = NULL, *email_ = NULL, *macaroon_ = NULL;
	g_autoptr(GPtrArray) discharges_ = NULL;
	g_autoptr(GError) error = NULL;

	parser = json_parser_new ();
	if (!json_parser_load_from_file (parser, filename, &error)) {
		if (!g_error_matches (error, G_FILE_ERROR, G_FILE_ERROR_NOENT))
			g_printerr ("Failed to load snapd auth: %s\n", error->message);
		return FALSE;
	}

	object = json_node_get_object (json_parser_get_root (parser));
	if (json_object_has_member (object, "id"))
		id_ = json_object_get_int_member (object, "id");
	if (json_object_has_member (object, "username"))
		username_ = json_object_get_string_member (object, "username");
	if (json_object_has_member (object, "email"))
		email_ = json_object_get_string_member (object, "email");
	if (json_object_has_member (object, "macaroon"))
		macaroon_ = json_object_get_string_member (object, "macaroon");
	discharges_ = g_ptr_array_new ();
	if (json_object_has_member (object, "discharges")) {
		JsonArray *array = json_object_get_array_member (object, "discharges");
		for (guint i = 0; i < json_array_get_length (array); i++)
			g_ptr_array_add (discharges_, json_array_get_string_element (array, i));
	}
	g_ptr_array_add (discharges_, NULL);

	if (id != NULL)
		*id = id_;
	if (username != NULL)
		*username = g_strdup (username_);
	if (email != NULL)
		*email = g_strdup (email_);
	if (macaroon != NULL)
		*macaroon = g_strdup (macaroon_);
	if (discharges != NULL)
		*discharges = g_strdupv ((GStrv) discharges_->pdata);

	return TRUE;
}

void
gs_snap_auth_save (gint64 id, const gchar *username, const gchar *email, const gchar *macaroon, GStrv discharges, GCancellable *cancellable)
{
	g_autofree gchar *filename = NULL;
	g_autofree gchar *dir = NULL;
	g_autoptr(JsonBuilder) builder = NULL;
	g_autoptr(JsonGenerator) generator = NULL;
	g_autoptr(JsonNode) root = NULL;
	g_autoptr(GFile) file = NULL;
	g_autoptr(GFileOutputStream) stream = NULL;
	g_autoptr(GError) error = NULL;

	filename = get_auth_filename ();

	dir = g_path_get_dirname (filename);
	g_mkdir_with_parents (dir, 0700);

	builder = json_builder_new ();
	json_builder_begin_object (builder);
	json_builder_set_member_name (builder, "id");
	json_builder_add_int_value (builder, id);
	if (username != NULL) {
		json_builder_set_member_name (builder, "username");
		json_builder_add_string_value (builder, username);
	}
	if (email != NULL) {
		json_builder_set_member_name (builder, "email");
		json_builder_add_string_value (builder, email);
	}
	if (macaroon != NULL) {
		json_builder_set_member_name (builder, "macaroon");
		json_builder_add_string_value (builder, macaroon);
	}
	if (discharges != NULL && g_strv_length (discharges) > 0) {
		int i;

		json_builder_set_member_name (builder, "discharges");
		json_builder_begin_array (builder);
		for (i = 0; discharges[i] != NULL; i++)
			json_builder_add_string_value (builder, discharges[i]);
		json_builder_end_array (builder);
	}
	json_builder_end_object (builder);

	generator = json_generator_new ();
	root = json_builder_get_root (builder);
	json_generator_set_root (generator, root);
	file = g_file_new_for_path (filename);
	stream = g_file_create (file, G_FILE_CREATE_PRIVATE, cancellable, &error);
	if (stream == NULL) {
		g_warning ("Failed to create auth file: %s", error->message);
		return;
	}
	if (!json_generator_to_stream (generator, G_OUTPUT_STREAM (stream), cancellable, &error))
		g_warning ("Failed to write auth file: %s", error->message);
}

void
gs_snap_auth_remove (void)
{
	g_autofree gchar *filename = get_auth_filename ();
	g_unlink (filename);
}

GFileMonitor *
gs_snap_auth_monitor (GCancellable *cancellable, GError **error)
{
	g_autofree gchar *filename = NULL;
	g_autoptr(GFile) file = NULL;

	filename = get_auth_filename ();
	file = g_file_new_for_path (filename);

	return g_file_monitor_file (file, G_FILE_MONITOR_NONE, cancellable, error);
}
